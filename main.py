from movierental.movie import Movie, REGULAR, NEW_RELEASE, CHILDRENS
from movierental.rental import Rental
from movierental.customer import Customer



star_war = Movie("Star wars I", REGULAR)
bambi = Movie("Bambi", CHILDRENS)
modern = Movie("Modern Film", NEW_RELEASE)



customer = Customer("Jhon Doe")
customer.add_rental(Rental(star_war, 5))
customer.add_rental(Rental(bambi, 7))
customer.add_rental(Rental(modern, 3))

print(customer.statement())
