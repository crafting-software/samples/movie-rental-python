

#
# The rental class represents a customer renting a movie.
#
class Rental:

    def __init__(self, movie, days_rented):
        self.movie = movie
        self.days_rented = days_rented
